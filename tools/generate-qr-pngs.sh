#!/bin/sh -e

basedir=$(cd "$(dirname "$0")/.."; pwd)
cd $basedir

for url in `grep -Eo 'https?://[^"]+/fdroid/repo\?fingerprint=[A-Z0-9]+' repo/index.html |sort -u`;  do
    filename=`echo $url | sed -E -e 's,https?://,,' -e 's,/,_,g' -e 's,\?fingerprint=[A-Z0-9]+$,,'`-QR.png
    qr $url > $filename.tmp
    convert $filename.tmp -scale 50% $filename
    rm $filename.tmp
    exiftool -all= -overwrite_original $filename
    if [ $filename = "guardianproject.info_fdroid_repo-QR.png" ]; then
        continue  # this is in index.png
    fi
    if ! grep -F $filename repo/index.html; then
	echo "$filename missing in index.html"
	exit 1
    fi
done
